import {
    GET_NEWS,
    CREATE_NEWS,
    DELETE_NEWS,
    NEWS_LOADING
} from '../actions/types';

const initialState = {
    allNews: JSON.parse(localStorage.getItem('news')),
    news: {},
    loading: false
};

export default function(state = initialState, action){
    switch(action.type){
        case NEWS_LOADING:
            return {
                ...state,
                loading: true
            };
        case GET_NEWS:
            return {
                ...state,
                news: state.allNews.find(news => news.id == action.payload),
                loading: false
            };
        case CREATE_NEWS:
            return {
                ...state,
                allNews: [action.payload, ...state.allNews]
            };
        case DELETE_NEWS:
            return {
                ...state,
                allNews: state.allNews.filter(news => news.id !== action.payload)
            };
        default:
            return state;
    }
}