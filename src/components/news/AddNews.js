import React, { Component } from 'react';
import { addNews } from '../../actions/newsActions';
import { connect } from 'react-redux';
import TextFieldGroup from '../common/TextFieldGroup';
import TextAreaFieldGroup from '../common/TextAreaFieldGroup';
import DateTimePicker from 'react-datetime-picker';

class AddNews extends Component {
    constructor(props){
        super(props);
        this.state = {
            id: 0,
            headline: '',
            description: '',
            author: '',
            date: new Date(),
            completed: false
        }

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    randomInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    onSubmit(e){
        e.preventDefault();
        
        const newNews = {
            id: this.randomInteger(1, 100000000),
            headline: this.state.headline,
            description: this.state.description,
            author: this.state.author,
            date: this.state.date
        }

        let result = !Object
            .values({ headline: this.state.headline, description: this.state.description, author: this.state.author})
            .some(o => o === '');

        if(result){
            this.props.addNews(newNews);
            this.props.history.push("/");
            this.setState({
                completed: false
            })
        } else {
            this.setState({
                completed: true
            })
        }
    }

    onChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }

    onDateChange = date => this.setState({ date })

    render(){
        return(
            <div className="container">
                <div className="row">
                    <div className="col-md-10 m-auto">
                        <h1 className="display-4 text-center">Create News</h1>
                        {this.state.completed && <p className="text-danger text-center">Fill all the Fields</p>}
                        <form onSubmit={this.onSubmit}>
                            <TextFieldGroup
                                placeholder="Headline"
                                name="headline"
                                value={this.state.headline}
                                onChange={this.onChange}
                                info="A headline for your news"
                            />
                            <TextAreaFieldGroup
                                placeholder="Description"
                                name="description"
                                value={this.state.description}
                                onChange={this.onChange}
                                info="Tell us a little about this news"
                            />
                            <div className="row">
                                <div className="col-md-6 m-auto">
                                    <TextFieldGroup
                                        placeholder="Author"
                                        name="author"
                                        value={this.state.author}
                                        onChange={this.onChange}
                                        info="Name of the author for this news"
                                    />
                                </div>
                                <div className="col-md-6 m-auto">
                                    <DateTimePicker
                                        name="date"
                                        value={this.state.date}
                                        onChange={this.onDateChange}
                                    />
                                </div>
                            </div>
                            <input
                                type="submit"
                                value="Create"
                                className="btn btn-info btn-block mt-4"
                            />
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    news: state.news
});

export default connect(mapStateToProps, { addNews })(AddNews);