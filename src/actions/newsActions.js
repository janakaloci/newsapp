import {
    GET_NEWS,
    CREATE_NEWS,
    DELETE_NEWS,
    NEWS_LOADING
} from './types';

// Get News
export const getNews = id => dispatch => {
    dispatch(setNewsLoading);
    dispatch({ type: GET_NEWS, payload: id });
}

// Create News
export const addNews = newsData => dispatch => {
    let news = JSON.parse(localStorage.getItem('news'));
    news.push(newsData);
    localStorage.setItem('news', JSON.stringify(news));
    dispatch({ type: CREATE_NEWS, payload: newsData });
}

// Delete News
export const deleteNews = id => dispatch => {
    let news = JSON.parse(localStorage.getItem('news'));
    news = news.filter(news => news.id !== id);
    localStorage.setItem('news', JSON.stringify(news));
    dispatch({ type: DELETE_NEWS, payload: id });
}

// Set loading state
export const setNewsLoading = () => {
    return {
      type: NEWS_LOADING
    };
};