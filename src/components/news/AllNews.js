import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import moment from 'moment'

class AllNews extends Component {
    getNews = () => {
        const { allNews } = this.props.news;

        if(allNews.length === 0){
            return (
                <h5>Sorry, we have got no news right now, try adding one.</h5>
            );
        } else {
            return allNews.map(news => {
                return (
                    <div key={news.id} className="card">
                        <h4>{news.headline}</h4>
                        <p>{news.description.length > 20 ? news.description.substring(0, 10) + '...' : news.description}</p>
                        <div style={{display: 'flex', justifyContent: 'space-between'}}>
                            <p>By {news.author}</p>
                            <p>{moment(news.date).format("YYYY-MM-DD HH:mm:ss")}</p>
                        </div>
                        <Link className="btn btn-dark btn-sm my-1" to={`/${news.id}`}>Read More</Link>
                    </div>
                )
            })
        }
    }

    render(){
        return(
            <>
                <div>
                    <Link className="btn btn-dark btn-sm my-1" to={`/create`}>Add News</Link>
                </div>
                {this.getNews()}
            </>
        )
    }
}

AllNews.propTypes = {
    news: PropTypes.object.isRequired
}
  
const mapStateToProps = state => ({
    news: state.news
});
  
  
export default connect(mapStateToProps, null)(AllNews);
  