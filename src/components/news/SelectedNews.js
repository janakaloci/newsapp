import React, { Component } from 'react';
import { getNews, deleteNews } from '../../actions/newsActions';
import { connect } from 'react-redux';
import moment from 'moment';
import { Link } from 'react-router-dom';

class SelectedNews extends Component {
    componentDidMount(){
        this.props.getNews(this.props.match.params.id);
    }

    onDelete = (id) => {
        this.props.deleteNews(id);
        this.props.history.push("/");
    }

    render(){
        const { news } = this.props.news;

        return(
            <>
            {news && <div className="container">
                <div className="row">
                    <div className="col-md-10 m-auto">
                        <h3>{news.headline}</h3>
                        <hr />
                        <p>{news.description}</p>
                        <div className="row">
                            <div className="col-md-6">
                                By {news.author}
                            </div>
                            <div className="col-md-6">
                                {moment(news.date).format("YYYY-MM-DD HH:mm:ss")}
                            </div>
                        </div>
                        <div className="card text-right">
                            <Link to="/" className="btn btn-dark btn-sm my-1">Go Back</Link>
                            <button className="btn btn-danger btn-sm my-1" onClick={() => this.onDelete(news.id)}>Delete</button>
                        </div>
                    </div>
                </div>
            </div>}
            </>
        )
    }
}

const mapStateToProps = state => ({
    news: state.news
});

export default connect(mapStateToProps, { getNews, deleteNews })(SelectedNews);