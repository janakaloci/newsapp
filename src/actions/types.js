export const GET_NEWS = 'GET_NEWS';
export const CREATE_NEWS = 'CREATE_NEWS';
export const DELETE_NEWS = 'DELETE_NEWS';
export const NEWS_LOADING = 'NEWS_LOADING';